#include <EEPROM.h>
#include <LittleFS.h>
#include "persistent_struct.h"
#include "string_utils.h"

#define RELAY_PIN_INDEX 0
#define RELAY_NAME_INDEX 1
#define RELAY_ASPECT_INDEX 1
#define RELAY_VALUE_INDEX 2

#define SCHED_NAME_INDEX 0
#define SCHED_ONETIME_INDEX 1

#define AUTOM_TIME_INDEX 0
#define AUTOM_INSTR_INDEX 1

#define SCHED_YEAR_INDEX 2
#define SCHED_MON_INDEX 3
#define SCHED_DATE_INDEX 4

#define SCHED_ONMON_INDEX 5
#define SCHED_ONTUE_INDEX 6
#define SCHED_ONWED_INDEX 7
#define SCHED_ONTHU_INDEX 8
#define SCHED_ONFRI_INDEX 9
#define SCHED_ONSAT_INDEX 10
#define SCHED_ONSUN_INDEX 11

#define SCHED_START_HIUR_INDEX 12
#define SCHED_START_MIN_INDEX 13
#define SCHED_DURATION_INDEX 14

#define MAX_PERSIST_LOG_LINES 10
#define MAX_LOG_FILE_SIZE_BYTES 500000 // .5 mb

Config storedConfig = {
  "","","EET-2EEST,M3.5.0/3,M10.5.0/4",
  -1,-1,-1,-1,-1,-1,
  
  "",-1,"","","","","",
  
  false,"","",
  
  "","","","",
  
  0,0,0,0
};

Schedule *storedSchedules;
Relay *storedRelays;
RelaySchedule *storedRelaySchedules;
AutomataStep *storedAutomataSteps;


/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * PERSISTANE READ WRITE OPERATIONS
**/

void persistLog(String logLine) {  
  File logFile = LittleFS.open("/fileLog1.txt", "a");    
  if(logFile.size() > MAX_LOG_FILE_SIZE_BYTES) {
    logFile.close();
    LittleFS.rename("/fileLog1.txt", "/fileLog2.txt");
    File logFile = LittleFS.open("/fileLog1.txt", "a");    
  }
  
  logFile.println(logLine);
  logFile.close();
}

//read a null terminated string of max length from the eeprom
String loadStringFromEeprom(int offset, int maxLen) {  
  char readStr[maxLen];   
  unsigned char chr;  
  int i;
  for(i = 0; (i < maxLen) && ((chr = EEPROM.read(offset + i)) != '\0'); i++) {
    readStr[i] = chr;    
  } 
  readStr[i] = '\0';
  return String(readStr);
}

//read a null terminated string to the eeprom
void saveStringToEeprom(int offset, String writeStr) {  
  for(int i = 0; i < writeStr.length(); i++) {
    EEPROM.write(offset + i, writeStr[i]);
  }
  EEPROM.write(offset + writeStr.length(), '\0');
  EEPROM.commit();
}

//read all configurations from the eeprom
void loadConfigFromEeprom(int offset) {  
  int eepromPos = offset;
  
  //first we read the general config - this will contain counts for the other stuff
  EEPROM.get(eepromPos, storedConfig);
  eepromPos += sizeof(Config);
  
  //read the required amount of relays
  storedRelays = (Relay*)realloc(storedRelays, storedConfig.relayCount * sizeof(Relay));    
  for(int i = 0; i < storedConfig.relayCount; i++) {
    EEPROM.get(eepromPos, storedRelays[i]);    
    storedRelays[i].impulse = 0;
    eepromPos += sizeof(Relay);
  }

  //read the required amount of schedules
  storedSchedules = (Schedule*)realloc(storedSchedules, storedConfig.scheduleCount * sizeof(Schedule));
  for(int i = 0; i < storedConfig.scheduleCount; i++) {
    EEPROM.get(eepromPos, storedSchedules[i]);
    eepromPos += sizeof(Schedule);
  }

  //read the required amount of relay / schedule assotiations
  storedRelaySchedules = (RelaySchedule*)realloc(storedRelaySchedules, storedConfig.relayScheduleCount * sizeof(RelaySchedule));
  for(int i = 0; i < storedConfig.relayScheduleCount; i++) {
    EEPROM.get(eepromPos, storedRelaySchedules[i]);
    eepromPos += sizeof(RelaySchedule);
  }

  //read the required amount of automata
  storedAutomataSteps = (AutomataStep*)realloc(storedAutomataSteps, storedConfig.automataCount * sizeof(AutomataStep));
  for(int i = 0; i < storedConfig.automataCount; i++) {
    EEPROM.get(eepromPos, storedAutomataSteps[i]);
    eepromPos += sizeof(AutomataStep);
  }
}

//save all configurations to the eeprom
void saveConfigToEeprom(int offset) {  
  int eepromPos = offset;
  EEPROM.put(eepromPos, storedConfig);  
  eepromPos += sizeof(Config);

  for(int i = 0; i < storedConfig.relayCount; i++) {
    EEPROM.put(eepromPos, storedRelays[i]);
    eepromPos += sizeof(Relay);
  }
  for(int i = 0; i < storedConfig.scheduleCount; i++) {
    EEPROM.put(eepromPos, storedSchedules[i]);
    eepromPos += sizeof(Schedule);
  }
  for(int i = 0; i < storedConfig.relayScheduleCount; i++) {
    EEPROM.put(eepromPos, storedRelaySchedules[i]);
    eepromPos += sizeof(RelaySchedule);
  }
  for(int i = 0; i < storedConfig.automataCount; i++) {
    EEPROM.put(eepromPos, storedAutomataSteps[i]);
    eepromPos += sizeof(AutomataStep);
  }

  EEPROM.commit();
}

/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * SPECIFIC GETTERS
**/

boolean isScheduleActive(int scheduleId) {
  time_t now = time(nullptr);  
  struct tm * timeinfo = localtime(&now);
    
  Schedule schedule = storedSchedules[scheduleId];
  boolean dateMatch = false;             
  if(schedule.oneTime) {    
    dateMatch = (schedule.year == (1900 + timeinfo->tm_year)) && (schedule.month == (timeinfo->tm_mon + 1)) && (schedule.date == timeinfo->tm_mday);
  } else {
    int weekday = timeinfo->tm_wday;
    dateMatch = (weekday == 0 && schedule.onSunday)
                || (weekday == 1 && schedule.onMonday)
                || (weekday == 2 && schedule.onTuesday)
                || (weekday == 3 && schedule.onWednesday)
                || (weekday == 4 && schedule.onThursday)
                || (weekday == 5 && schedule.onFriday)
                || (weekday == 6 && schedule.onSaturday);
  }
  
  int sstartt = schedule.startHour * 3600 + schedule.startMinute * 60;
  int sendt = sstartt + schedule.durationSeconds;
  int nowt = timeinfo->tm_hour * 3600 + timeinfo->tm_min * 60 + timeinfo->tm_sec;
  
  boolean timeIntervalMatch = sstartt <= nowt && nowt < sendt;
  
  return (dateMatch && timeIntervalMatch);
}

/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * MEMORY STRUCTURE UPDATE OPERATIONS
**/

void addRelaySchedule(int relayId, int scheduleId) {
  boolean foundRelaySchedule = false;
  boolean foundRelay = false;
  
  for(int i = 0; i < storedConfig.relayCount; i++) {
    if(storedRelays[i].pin == relayId) {
      foundRelay = true;
      break;
    }
  }
    
  if(foundRelay && scheduleId >= 0 && scheduleId < storedConfig.scheduleCount) {    
    for(int i = 0; i < storedConfig.relayScheduleCount; i++) {
      if(storedRelaySchedules[i].relayId == relayId && storedRelaySchedules[i].scheduleId == scheduleId) {
        foundRelaySchedule = true;
        break;
      }
    }
    
    if(!foundRelaySchedule) {
      RelaySchedule relaySchedule;
      relaySchedule.relayId = relayId;
      relaySchedule.scheduleId = scheduleId;
  
      storedConfig.relayScheduleCount++;
      storedRelaySchedules = (RelaySchedule *)realloc(storedRelaySchedules, sizeof(RelaySchedule)*storedConfig.relayScheduleCount);
      storedRelaySchedules[storedConfig.relayScheduleCount - 1] = relaySchedule; 
    }
  }  
}

boolean delRelaySchedule(int relayId, int scheduleId) {  
  boolean foundRelaySchedule = false; 
  
  for(int i = 0; i < storedConfig.relayScheduleCount; i++) {
    foundRelaySchedule |= (relayId == -1 || storedRelaySchedules[i].relayId == relayId)
                          && 
                          (scheduleId == -1 || storedRelaySchedules[i].scheduleId == scheduleId);
                  
    if(foundRelaySchedule && i < (storedConfig.relayScheduleCount - 1)) {
      storedRelaySchedules[i] = storedRelaySchedules[i + 1];
    }
  }
  
  if(foundRelaySchedule) {
    storedConfig.relayScheduleCount--;
    storedRelaySchedules = (RelaySchedule *)realloc(storedRelaySchedules, sizeof(RelaySchedule)*storedConfig.relayScheduleCount);      
  }
  
  return foundRelaySchedule;
}

void addScheduleFromString(String schedule) {

  if(schedule != "") {  
    String decodedSchedule = urldecode(schedule);
    Schedule schedule;
    
    String scheduleName = urldecode(split(decodedSchedule, ',', SCHED_NAME_INDEX));     
    scheduleName.toCharArray(schedule.name, scheduleName.length() + 1, 0);
    schedule.oneTime = split(decodedSchedule, ',', SCHED_ONETIME_INDEX) == "true";

    schedule.onMonday = split(decodedSchedule, ',', SCHED_ONMON_INDEX) == "true";
    schedule.onTuesday = split(decodedSchedule, ',', SCHED_ONTUE_INDEX) == "true";
    schedule.onWednesday = split(decodedSchedule, ',', SCHED_ONWED_INDEX) == "true";
    schedule.onThursday = split(decodedSchedule, ',', SCHED_ONTHU_INDEX) == "true";
    schedule.onFriday = split(decodedSchedule, ',', SCHED_ONFRI_INDEX) == "true";
    schedule.onSaturday = split(decodedSchedule, ',', SCHED_ONSAT_INDEX) == "true";
    schedule.onSunday = split(decodedSchedule, ',', SCHED_ONSUN_INDEX) == "true";

    schedule.year = split(decodedSchedule, ',', SCHED_YEAR_INDEX).toInt();
    schedule.month = split(decodedSchedule, ',', SCHED_MON_INDEX).toInt();
    schedule.date = split(decodedSchedule, ',', SCHED_DATE_INDEX).toInt();

    schedule.startHour = split(decodedSchedule, ',', SCHED_START_HIUR_INDEX).toInt();
    schedule.startMinute = split(decodedSchedule, ',', SCHED_START_MIN_INDEX).toInt();
    schedule.durationSeconds = split(decodedSchedule, ',', SCHED_DURATION_INDEX).toInt();    
  
    if(schedule.oneTime && schedule.year > 0 && schedule.month > 0 &&  schedule.date > 0) {
      schedule.onMonday = schedule.onTuesday = schedule.onWednesday = schedule.onThursday = schedule.onFriday = schedule.onSaturday = schedule.onSunday = false;
    }
  
    if(schedule.onMonday || schedule.onTuesday || schedule.onWednesday || schedule.onThursday || schedule.onFriday || schedule.onSaturday || schedule.onSunday) {
      schedule.oneTime = false;
      schedule.year = 0;
      schedule.month = 0;
      schedule.date = 0;
    }
  
    storedConfig.scheduleCount++;
    storedSchedules = (Schedule *)realloc(storedSchedules, sizeof(Schedule)*storedConfig.scheduleCount);
    storedSchedules[storedConfig.scheduleCount - 1] = schedule;  
  }
}

void delSchedule(int scheduleId) {
  if(scheduleId >=0 && scheduleId < storedConfig.scheduleCount) {
    storedConfig.scheduleCount--;
    for(int i = scheduleId; i < storedConfig.scheduleCount; i++) {
      storedSchedules[i] = storedSchedules[i + 1];
    }
    storedSchedules = (Schedule *)realloc(storedSchedules, sizeof(Schedule)*storedConfig.scheduleCount);     
  }
  while(delRelaySchedule(-1, scheduleId)) {
    //no op
  }
}

void clearAutomataSteps() {
  storedConfig.automataCount = 0;
  storedAutomataSteps = (AutomataStep *)realloc(storedAutomataSteps, sizeof(AutomataStep)*storedConfig.automataCount);
}

void addAutomataStep(long timeOffsetMs, String multiInstr) {
  AutomataStep automataStep;
  storedConfig.automataCount++;  
  
  automataStep.timeOffsetMs = timeOffsetMs;
  multiInstr.toCharArray(automataStep.multiInstr, multiInstr.length() + 1, 0);
   
  storedAutomataSteps = (AutomataStep *)realloc(storedAutomataSteps, sizeof(AutomataStep)*storedConfig.automataCount);
  storedAutomataSteps[storedConfig.automataCount - 1] = automataStep;
}

void addRelaysFromString(String relays) {  
  int previousSize = storedConfig.relayCount;
  int previousPins[previousSize];   
  for(int i = 0; i < previousSize; i++) {
    previousPins[i] = storedRelays[i].pin;
  }
  
  if(relays == "") {
    storedConfig.relayCount = 0;      
  } else {
    String decodedRelays = urldecode(relays);    
    String decodedRelay;  
    int count = 0;
    while((decodedRelay = split(decodedRelays, '|', count)) != "") {      
      Relay relay = {
          0,"",
          false,false, false, false, 0,
          false, false,
          ""        
      };           
  
      relay.pin = split(decodedRelay, ',', RELAY_PIN_INDEX).toInt();
      String name = urldecode(split(decodedRelay, ',', RELAY_NAME_INDEX)); 
      name.toCharArray(relay.name, name.length() + 1, 0); 

      count++;
      storedConfig.relayCount = count;      
      storedRelays = (Relay *)realloc(storedRelays, sizeof(Relay) * storedConfig.relayCount);
      storedRelays[storedConfig.relayCount - 1] = relay;
    }
  }

  for(int i = 0; i < storedConfig.relayCount; i++) {
     for(int j = 0; j < previousSize; j++) {
        if(previousPins[j] == storedRelays[i].pin) {
          previousPins[j] = -1;
        }
     }
  }

  for(int j = 0; j < previousSize; j++) {
    if(previousPins[j] != -1) {
      while(delRelaySchedule(previousPins[j], -1)) {
        //no op
      }
    }
  }
}

void setAutomataFromString(String automata) {  
  clearAutomataSteps();
  if(automata != "") {
    String decodedAutomata = urldecode(automata);    
    String decodedStep;
    int count = 0;
    while((decodedStep = split(decodedAutomata, '|', count)) != "") {        
      addAutomataStep(strtoul(split(decodedStep, ' ', AUTOM_TIME_INDEX).c_str(), NULL, 10), urldecode(split(decodedStep, ' ', AUTOM_INSTR_INDEX)));          
      count++;
    }
  }
}

void setRelayFromString(String relay) {  
  if(relay != "") {        
    String decodedRelay = urldecode(relay);        
    int relayId = split(decodedRelay, ',', RELAY_PIN_INDEX).toInt();
    String aspectName = split(decodedRelay, ',', RELAY_ASPECT_INDEX);
    String aspectValue = splitTail(decodedRelay, ',', RELAY_VALUE_INDEX, true);

    for (int i = 0; i < storedConfig.relayCount; i++) {
      if(storedRelays[i].pin == relayId) {   
        if (aspectName == "mqtt") {
          String value = urldecode(aspectValue);                    
          value.toCharArray(storedRelays[i].mqtt, value.length() + 1, 0);          
        }
        if (aspectName == "impulse") {
          storedRelays[i].impulse = aspectValue.toInt();            
        }
        if (aspectName == "on") {            
          storedRelays[i].on = (aspectValue == "true");            
        }
        if (aspectName == "schedule") {            
          storedRelays[i].schedule = (aspectValue == "true");            
        }
        if (aspectName == "pir") {
          storedRelays[i].pir = (aspectValue == "true");
          if(storedRelays[i].pir) {
            storedRelays[i].heat = false;
            storedRelays[i].cool = false;
          }
        }       
        if (aspectName == "heat") {
          storedRelays[i].heat = (aspectValue == "true");
          if(storedRelays[i].heat) {            
            storedRelays[i].pir = false;
            storedRelays[i].cool = false;
          }
        }
        if (aspectName == "cool") {
          storedRelays[i].cool = (aspectValue == "true");
          if(storedRelays[i].cool) {            
            storedRelays[i].pir = false;
            storedRelays[i].heat = false;
          }
        }
        if (aspectName == "temptarget") {
          storedRelays[i].temptarget = aspectValue.toInt();
        }    
      }
    } 
  }  
}

boolean pullFromInstruction(String name, String value) {
  if (name == "DELSCHEDULE") {
    int index = value.toInt();
    delSchedule(index);
    return true;
  }  
  if (name == "ADDSCHEDULE") {          
    addScheduleFromString(value);        
    return true;
  } 
  if (name == "ADDRELAYSCHEDULE") {                  
    int relayId = split(value, ',', 0).toInt();
    int scheduleId = split(value, ',', 1).toInt();
    addRelaySchedule(relayId, scheduleId);          
    return true;
  }          
  if (name == "DELRELAYSCHEDULE") {                       
    int relayId = split(value, ',', 0).toInt();
    int scheduleId = split(value, ',', 1).toInt();
    delRelaySchedule(relayId, scheduleId);  
    return true;
  } 
  if (name == "SETRELAY") {          
    setRelayFromString(value);
    return true;
  }
  if (name == "SETAUTOMATA") {
    setAutomataFromString(value);
    return true;
  }
  
  return false;
}

boolean pullConfigFromInstruction(String name, String value) {
  if (name == "IDENTITY") {
    value = urldecode(value);
    value.toCharArray(storedConfig.identity, value.length() + 1, 0);
    return true;
  }
  if (name == "METADATA") {
    value = urldecode(value);
    value.toCharArray(storedConfig.metadata, value.length() + 1, 0);
    return true;
  }
  if (name == "TIMEZONE") {        
    value = urldecode(value);
    value.toCharArray(storedConfig.timezone, value.length() + 1, 0);
    setenv("TZ", storedConfig.timezone, 1);
    tzset();
    return true;
  }      
  if (name == "MQTTURL") {        
    urldecode(value).toCharArray(storedConfig.mqtturl, value.length() + 1, 0);
    return true;
  }
  if (name == "MQTTPORT") {  
    storedConfig.mqttport = value.toInt();    
    return true;
  }
  if (name == "MQTTUSER") {
    urldecode(value).toCharArray(storedConfig.mqttuser, value.length() + 1, 0);
    return true;
  }
  if (name == "MQTTPASS") {        
    urldecode(value).toCharArray(storedConfig.mqttpass, value.length() + 1, 0);
    return true;    
  }      
  if (name == "MQTTBASEPATH") {        
    urldecode(value).toCharArray(storedConfig.mqttbasepath, value.length() + 1, 0);    
    return true;    
  }  
  if (name == "MQTTCMDCHANNEL") {        
    urldecode(value).toCharArray(storedConfig.mqttcmdchannel, value.length() + 1, 0);    
    return true;    
  }  
  if (name == "MQTTCMDJSON") {        
    urldecode(value).toCharArray(storedConfig.mqttcmdjson, value.length() + 1, 0);    
    return true;    
  } 
  if (name == "ALLWAYSAP") {
    storedConfig.allwaysap = (value == "true");
    return true;
  }
  if (name == "SSID") {
    value.toCharArray(storedConfig.ssid, value.length() + 1, 0);
    return true;
  }
  if (name == "PASS") {
    value.toCharArray(storedConfig.pass, value.length() + 1, 0);
    return true;
  }
  if (name == "IP") {
    value.toCharArray(storedConfig.ip, value.length() + 1, 0);
    return true;
  }
  if (name == "GATEWAY") {
    value.toCharArray(storedConfig.gateway, value.length() + 1, 0);
    return true;
  }
  if (name == "DNS") {
    value.toCharArray(storedConfig.dns, value.length() + 1, 0);
    return true;
  }
  if (name == "NETMASK") {
    value.toCharArray(storedConfig.netmask, value.length() + 1, 0);
    return true;
  }  
  if (name == "piezopin") {
    storedConfig.piezopin = value.toInt();  
    return true;      
  }  
  if (name == "dhtpin") {
    storedConfig.dhtpin = value.toInt();  
    return true;      
  }
  if (name == "pirpin") {
    storedConfig.pirpin = value.toInt(); 
    return true;       
  }
  if (name == "steppersteppin") {
    storedConfig.steppersteppin = value.toInt(); 
    return true;       
  }
  if (name == "stepperdirpin") {
    storedConfig.stepperdirpin = value.toInt(); 
    return true;       
  }
  if (name == "stepperenablepin") {
    storedConfig.stepperenablepin = value.toInt(); 
    return true;       
  }
  if (name.startsWith("relays")) {
    addRelaysFromString(value);       
    return true;
  }

  return false;
}

void clearWifiData() {  
  storedConfig.allwaysap = false;
  String("").toCharArray(storedConfig.ssid, 1, 0);
  String("").toCharArray(storedConfig.pass, 1, 0);  
}

void forceWifiData(String ssid, String pass) {    
  String("").toCharArray(storedConfig.ip, 1, 0);
  String("").toCharArray(storedConfig.gateway, 1, 0);
  String("").toCharArray(storedConfig.netmask, 1, 0);
  String("").toCharArray(storedConfig.dns, 1, 0);  

  ssid.toCharArray(storedConfig.ssid, ssid.length() + 1, 0);
  pass.toCharArray(storedConfig.pass, pass.length() + 1, 0);  
}


/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * TEMPLATE FILL OPERATIONS
**/

String templateFillRelay(String tmplt, int relayIndex) {
  tmplt.replace("@@schedule@@", storedRelays[relayIndex].schedule ? "true" : "false"); 
  tmplt.replace("@@pir@@", storedRelays[relayIndex].pir ? "true" : "false");   
  tmplt.replace("@@heat@@", storedRelays[relayIndex].heat ? "true" : "false"); 
  tmplt.replace("@@cool@@", storedRelays[relayIndex].cool ? "true" : "false"); 
  tmplt.replace("@@temptarget@@", String(storedRelays[relayIndex].temptarget));   
  tmplt.replace("@@on@@", storedRelays[relayIndex].on ? "true" : "false");
  tmplt.replace("@@impulse@@", (storedRelays[relayIndex].impulse == 0) ? "false" : "true");
  tmplt.replace("@@pin@@", String(storedRelays[relayIndex].pin));    
  tmplt.replace("@@name@@", storedRelays[relayIndex].name);
  tmplt.replace("@@mqtt@@", storedRelays[relayIndex].mqtt);
  return tmplt;
}

String templateFillRelayArray(String tmplt, String prefix, String suffix, String separator) {
  String response = prefix;
  
  for (int i = 0; i < storedConfig.relayCount; i++) {      
    String s = tmplt;
    s = templateFillRelay(s, i);
    response += ((i > 0) ? separator : "") + s;    
  }    
  
  response += suffix;
  
  return response;
}

String templateFillSchedule(String tmplt, int scheduleIndex) { 
  tmplt.replace("@@id@@", String(scheduleIndex));
  tmplt.replace("@@name@@", storedSchedules[scheduleIndex].name);
  tmplt.replace("@@oneTime@@", storedSchedules[scheduleIndex].oneTime ? "true" : "false");    
  
  tmplt.replace("@@onMonday@@", storedSchedules[scheduleIndex].onMonday ? "true" : "false");
  tmplt.replace("@@onTuesday@@", storedSchedules[scheduleIndex].onTuesday ? "true" : "false");
  tmplt.replace("@@onWednesday@@", storedSchedules[scheduleIndex].onWednesday ? "true" : "false");
  tmplt.replace("@@onThursday@@", storedSchedules[scheduleIndex].onThursday ? "true" : "false");
  tmplt.replace("@@onFriday@@", storedSchedules[scheduleIndex].onFriday ? "true" : "false");
  tmplt.replace("@@onSaturday@@", storedSchedules[scheduleIndex].onSaturday ? "true" : "false");
  tmplt.replace("@@onSunday@@", storedSchedules[scheduleIndex].onSunday ? "true" : "false");
  
  tmplt.replace("@@year@@", String(storedSchedules[scheduleIndex].year));
  tmplt.replace("@@month@@", String(storedSchedules[scheduleIndex].month));
  tmplt.replace("@@date@@", String(storedSchedules[scheduleIndex].date));
  
  tmplt.replace("@@startHour@@", String(storedSchedules[scheduleIndex].startHour));
  tmplt.replace("@@startMinute@@", String(storedSchedules[scheduleIndex].startMinute));
  tmplt.replace("@@durationSeconds@@", String(storedSchedules[scheduleIndex].durationSeconds));  

  return tmplt;
}

String templateFillScheduleArray(String tmplt, String prefix, String suffix, String separator) {
  String response = prefix;
  
  for (int i = 0; i < storedConfig.scheduleCount; i++) {      
      String s = tmplt;
      s = templateFillSchedule(s, i);            
      response += ((i > 0)? separator : "") + s;   
  }    
  
  response += suffix;
  
  return response;
}

String templateFillRelaySchedule(String tmplt, int scheduleIndex) {
  tmplt.replace("@@relayId@@", String(storedRelaySchedules[scheduleIndex].relayId));
  tmplt.replace("@@scheduleId@@", String(storedRelaySchedules[scheduleIndex].scheduleId));
  return tmplt;
}

String templateFillAutomata(String tmplt, int automataIndex) {
  tmplt.replace("@@timeOffset@@", String(storedAutomataSteps[automataIndex].timeOffsetMs));
  tmplt.replace("@@multiInst@@", storedAutomataSteps[automataIndex].multiInstr);
  return tmplt;
}

String templateFillRelayScheduleArray(String tmplt, String prefix, String suffix, String separator) {
  String response = prefix;
  
  for (int i = 0; i < storedConfig.relayScheduleCount; i++) {      
      String s = tmplt;
      s= templateFillRelaySchedule(s, i);      
      response += ((i > 0)? separator : "") + s;   
  }    
  
  response += suffix;

  return response;
}

String templateFillAutomataArray(String tmplt, String prefix, String suffix, String separator) {
  String response = prefix;
  
  for (int i = 0; i < storedConfig.automataCount; i++) {      
      String s = tmplt;
      s= templateFillAutomata(s, i);      
      response += ((i > 0)? separator : "") + s;   
  }    
  
  response += suffix;

  return response;
}

String templateFillConfig(String tmplt) {
  
  tmplt.replace("@@identity@@", storedConfig.identity);
  tmplt.replace("@@metadata@@", storedConfig.metadata);
  tmplt.replace("@@timezone@@", String(storedConfig.timezone));
  
  tmplt.replace("@@dht@@", (storedConfig.dhtpin != -1) ? "true" : "false");    
  tmplt.replace("@@dhtpin@@", String(storedConfig.dhtpin));

  tmplt.replace("@@piezo@@", (storedConfig.piezopin != -1) ? "true" : "false");
  tmplt.replace("@@piezopin@@", String(storedConfig.piezopin));
  
  tmplt.replace("@@stepper@@", (storedConfig.steppersteppin != -1) ? "true" : "false");  
  tmplt.replace("@@steppersteppin@@", String(storedConfig.steppersteppin));
  tmplt.replace("@@stepperdirpin@@", String(storedConfig.stepperdirpin));
  tmplt.replace("@@stepperenablepin@@", String(storedConfig.stepperenablepin));
  
  tmplt.replace("@@pir@@", (storedConfig.pirpin != -1) ? "true" : "false");  
  tmplt.replace("@@pirpin@@", String(storedConfig.pirpin));    
  
  tmplt.replace("@@mqtturl@@", urlencode(storedConfig.mqtturl));
  tmplt.replace("@@mqttport@@", String(storedConfig.mqttport));
  tmplt.replace("@@mqttuser@@", urlencode(storedConfig.mqttuser));
  tmplt.replace("@@mqttpass@@", urlencode(storedConfig.mqttpass));
  tmplt.replace("@@mqttbasepath@@", urlencode(storedConfig.mqttbasepath));
  tmplt.replace("@@mqttcmdchannel@@", urlencode(storedConfig.mqttcmdchannel));
  tmplt.replace("@@mqttcmdjson@@", urlencode(storedConfig.mqttcmdjson));

  tmplt.replace("@@allwaysap@@", storedConfig.allwaysap ? "true" : "false");
  tmplt.replace("@@ssid@@", storedConfig.ssid);
  tmplt.replace("@@pass@@", storedConfig.pass);
  
  tmplt.replace("@@ip@@", storedConfig.ip);
  tmplt.replace("@@gateway@@", storedConfig.gateway);
  tmplt.replace("@@dns@@", storedConfig.dns);
  tmplt.replace("@@netmask@@", storedConfig.netmask);
  tmplt.replace("@@relay@@", (storedConfig.relayCount > 0) ? "true" : "false");
  
  return tmplt;
}
