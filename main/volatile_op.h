#include "volatile_struct.h"

TempHumidData tempHumidData = {
  false,
  0,0,0,0,0,
  false, false, false, false
};

DateTimeData dateTimeData = {
  0,0,0,0,0,0,0,0
};

/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * UPDATE OPERATIONS
**/

void readClock() {  
  time_t now = time(nullptr);  
  struct tm * timeinfo = localtime(&now);
  
  dateTimeData.year = 1900 + timeinfo->tm_year;  
  dateTimeData.month = timeinfo->tm_mon + 1;
  dateTimeData.date = timeinfo->tm_mday;
  dateTimeData.weekday = timeinfo->tm_wday;
  dateTimeData.hour = timeinfo->tm_hour;
  dateTimeData.minute = timeinfo->tm_min;
  dateTimeData.second = timeinfo->tm_sec;    
  dateTimeData.timestamp = (unsigned) now;
}

void readSensor(DHTesp *dht) {   
  if(storedConfig.dhtpin != -1) {     
    tempHumidData.temperature = dht->getTemperature();
    tempHumidData.humidity = dht->getHumidity();
    
    if (!isnan(tempHumidData.temperature) && !isnan(tempHumidData.humidity)) {
      tempHumidData.senzorError = false;
      
      tempHumidData.heatindex = dht->computeHeatIndex(tempHumidData.temperature, tempHumidData.humidity, false);
      tempHumidData.dewpoint = dht->computeDewPoint(tempHumidData.temperature, tempHumidData.humidity, false);
      tempHumidData.comfortlevel = dht->computePerception(tempHumidData.temperature, tempHumidData.humidity, false);
  
      ComfortState comfort;
      dht->getComfortRatio(comfort, tempHumidData.temperature, tempHumidData.humidity, false);
  
      tempHumidData.toohot = comfort == Comfort_TooHot || comfort == Comfort_HotAndHumid || comfort == Comfort_HotAndDry;
      tempHumidData.toocold = comfort == Comfort_TooCold || comfort == Comfort_ColdAndHumid || comfort == Comfort_ColdAndDry;
      tempHumidData.toohumid = comfort == Comfort_TooHumid || comfort == Comfort_ColdAndHumid || comfort == Comfort_HotAndHumid;
      tempHumidData.toodry = comfort == Comfort_TooDry || comfort == Comfort_ColdAndDry || comfort == Comfort_HotAndDry;        
    } else {
      tempHumidData.senzorError = true;
    }
  } else {
    tempHumidData.senzorError = true;
  }
}

/**
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * **********************************************************************************************************************************
 * TEMPLATE FILL OPERATIONS
**/

String getFormattedDateTime() {
  return String(dateTimeData.year) + 
          "-" + 
          ((dateTimeData.month < 10) ? ("0" + String(dateTimeData.month)) : String(dateTimeData.month)) + 
          "-" + 
          ((dateTimeData.date < 10) ? ("0" + String(dateTimeData.date)) : String(dateTimeData.date)) + 
          " " + 
          ((dateTimeData.hour < 10) ? ("0" + String(dateTimeData.hour)) : String(dateTimeData.hour)) + 
          ":" +   
          ((dateTimeData.minute < 10) ? ("0" + String(dateTimeData.minute)) : String(dateTimeData.minute)) + 
          ":" + 
          ((dateTimeData.second < 10) ? ("0" + String(dateTimeData.second)) : String(dateTimeData.second));
}

String templateFillEnvironment(String tmplt) {
  tmplt.replace("@@identity@@", storedConfig.identity);
  tmplt.replace("@@datetime@@", getFormattedDateTime());    
 
  tmplt.replace("@@temperature@@", isnan(tempHumidData.temperature)? "0" : String(tempHumidData.temperature));
  tmplt.replace("@@heatindex@@", String(tempHumidData.heatindex));
  tmplt.replace("@@humidity@@", isnan(tempHumidData.humidity)? "0" : String(tempHumidData.humidity));
  tmplt.replace("@@dewpoint@@", String(tempHumidData.dewpoint));
  tmplt.replace("@@comfortlevel@@", String(tempHumidData.comfortlevel));

  tmplt.replace("@@toohot@@", tempHumidData.toohot ? "true" : "false");
  tmplt.replace("@@toocold@@", tempHumidData.toocold ? "true" : "false");
  tmplt.replace("@@toohumid@@", tempHumidData.toohumid ? "true" : "false");
  tmplt.replace("@@toodry@@", tempHumidData.toodry ? "true" : "false");    

  return tmplt;
}

String getTempHumidLogLine() {  
  if(dateTimeData.timestamp < 10000) {
    return "";
  } else {
    return "1," + String(dateTimeData.timestamp) + "," + (isnan(tempHumidData.temperature)? "0" : String((int)(tempHumidData.temperature * 10))) + "," + (isnan(tempHumidData.humidity)? "0" : String((int)(tempHumidData.humidity * 10)));  
  }
}

String getRelayLogLine() {  
  String logLine = "2," + String(dateTimeData.timestamp);
  
  for(int k = storedConfig.relayCount - 1; k >= 0; k--) {    
    logLine += "," + String(storedRelays[k].pin) + ":" + ((storedRelays[k].on || storedRelays[k].impulse != 0) ? "1" : "0");
  }  
  
  return "" + logLine;
}
