#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include <WiFiClientSecure.h>
#include <ESP8266HTTPClient.h>
#include <LittleFS.h>

#define HTTP_PORT 80
#define HTTPS_PORT 443

//send a file from flash if it was requested

void hanldeFileRequest(AsyncWebServerRequest *request, String path) {
  String dataType = "text/plain";   
  if(path.endsWith("/")) path += "index.htm";

  if(LittleFS.exists(path)) { 
    Serial.println("Getting file: " + path);
    
    if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
    else if(path.endsWith(".html")) dataType = "text/html";
    else if(path.endsWith(".htm")) dataType = "text/html";
    else if(path.endsWith(".css")) dataType = "text/css";
    else if(path.endsWith(".js")) dataType = "application/javascript";
    else if(path.endsWith(".png")) dataType = "image/png";
    else if(path.endsWith(".gif")) dataType = "image/gif";
    else if(path.endsWith(".jpg")) dataType = "image/jpeg";
    else if(path.endsWith(".ico")) dataType = "image/x-icon";
    else if(path.endsWith(".xml")) dataType = "text/xml";
    else if(path.endsWith(".pdf")) dataType = "application/pdf";
    else if(path.endsWith(".zip")) dataType = "application/zip";
    if (request->hasArg("download")) dataType = "application/octet-stream";
  
    AsyncWebServerResponse *response = request->beginResponse(LittleFS, path, dataType);
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
  } else {
    Serial.println("Missing file: " + path);
    
    request->send(404);    
  }
}

void hanldeDefaultRequest(AsyncWebServerRequest *request) {
  hanldeFileRequest(request, request->url());
}
