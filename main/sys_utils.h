#define ERRLOG_MAXLEN_CHARS 1000

String getDeviceUID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);    
  }
  return result;
}

String getWifiJsonList() {
  int n = WiFi.scanNetworks();
  String wifis = "[";
  
  for (int i = 0; i < n; i++) {
    wifis += '"' + WiFi.SSID(i) + '"';
    if (i < n - 1) {
      wifis += ", ";
    }
  }
  
  wifis += "]";  

  return wifis;
}

#define UNKNOWN_PIN 0xFF

uint8_t getPinMode(uint8_t pin)
{
  uint32_t bit = digitalPinToBitMask(pin);
  uint32_t port = digitalPinToPort(pin);

  // I don't see an option for mega to return this, but whatever...
  if (NOT_A_PIN == port) return UNKNOWN_PIN;

  // Is there a bit we can check?
  if (0 == bit) return UNKNOWN_PIN;

  // Is there only a single bit set?
  if (bit & bit - 1) return UNKNOWN_PIN;

  volatile uint32_t *reg, *out;
    
  reg = portModeRegister(port);
  out = portOutputRegister(port);

  if (*reg & bit)
    return OUTPUT;
  else if (*out & bit)
    return INPUT_PULLUP;
  else
    return INPUT;
}
