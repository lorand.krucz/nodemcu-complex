String splitTail(String data, char separator, int index, boolean squashed) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
      if (data.charAt(i) == separator || i == maxIndex) {
          found++;
          strIndex[0] = strIndex[1] + 1;
          strIndex[1] = (i == maxIndex) ? i+1 : i;
      }
  }
  return found > index ? data.substring(strIndex[0], squashed ? data.length() : strIndex[1]) : "";
}

String split(String data, char separator, int index) {
  return splitTail(data, separator, index, false);
}

String bufferToString(byte* data, int len) {
  char buf[len];
  memcpy(buf, data, len );
  buf[len] = '\0';
  return String(buf);
}

String urldecode(String param) {
  param.replace("+"," ");
  param.replace("%2B","+");
  
  param.replace("%0A","\n");
  param.replace("%0D","\r");
  param.replace("%20"," ");
  param.replace("%21","!");
  param.replace("%22","\"");
  param.replace("%23","#");
  param.replace("%24","$");
  param.replace("%26","&");
  param.replace("%27","'");
  param.replace("%28","(");
  param.replace("%29",")");
  param.replace("%2A","*");  
  param.replace("%2C",",");
  param.replace("%2F","/");
  param.replace("%3A",":");
  param.replace("%3B",";");
  param.replace("%3D","=");
  param.replace("%3F","?");
  param.replace("%40","@");
  param.replace("%5B","[");
  param.replace("%5D","]");
  param.replace("%7C","|");
  
  param.replace("%25","%");
  return param;
}

String urlencode(String param) {
  param.replace("%","%25");
  param.replace("+","%2B");
  param.replace(" ","+");
  
  param.replace("\n","%0A");
  param.replace("\r","%0D");
  param.replace(" ","%20");
  param.replace("!","%21");
  param.replace("\"","%22");
  param.replace("#","%23");
  param.replace("$","%24");
  param.replace("&","%26");
  param.replace("'","%27");
  param.replace("(","%28");
  param.replace(")","%29");
  param.replace("*","%2A");  
  param.replace(",","%2C");
  param.replace("/","%2F");
  param.replace(":","%3A");
  param.replace(";","%3B");
  param.replace("=","%3D");
  param.replace("?","%3F");
  param.replace("@","%40");
  param.replace("[","%5B");
  param.replace("]","%5D");
  param.replace("|","%7C");  
  return param;
}
