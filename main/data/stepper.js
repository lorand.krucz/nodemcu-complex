function enableStepper() {
	$('#stepper_container').show();	
}

function moveStepper(forward) {		
	var steps = $("#steps").val();
	var startperiod = $("#startperiod").val();
	var targetperiod = $("#targetperiod").val();
	var acceleration = $("#acceleration").val();
		
	motionParams = steps + "," + startperiod + "," + targetperiod + "," + acceleration + "," + forward;
	setApi = remote + "api?MVSTP=" + encodeURIComponent(motionParams);
	changed = true;
	monitorData();
}
	
function disableStepper() {
	setApi = remote + "api?DISSTP=true";
	changed = true;
	monitorData();
}