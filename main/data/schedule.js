var daysOfWeek = [["onMonday", "Mondays", false], ["onTuesday", "Tuesdays", false], ["onWednesday", "Wednesdays", false], ["onThursday", "Thursdays", false], ["onFriday", "Fridays", false], ["onSaturday", "Saturdays", false], ["onSunday", "Sundays", false]];

function enableSchedules() {		
	$("#add_schedule").show();
	$("#schedules_container").show();
	
	var daysOfWeekArray = {};	
	for(j = 0; j < daysOfWeek.length; j++) {
		daysOfWeekArray[j] = daysOfWeek[j][1];
	}
		
	$("#cb_oneTime").jqxCheckBox({ width: 100, height: 25});
	$("#schedule_days_select").jqxDropDownList({ checkboxes: true, source: daysOfWeekArray, width: 500});
				
	$("#schedule_date").jqxDateTimeInput({ width: '120px', height: '25px' });
	$("#schedule_from").jqxDateTimeInput({formatString: "HH:mm", showTimeButton: true, showCalendarButton: false, width: '90px', height: '25px' });	
	
	$("#schedule_days_select").on('checkChange', function (event) {
		if (event.args) {
			var item = event.args.item;
			daysOfWeek[item.value][2] = item.checked;  
			if(item.checked) {
				$("#cb_oneTime").jqxCheckBox({checked: false});
			}
		}
	});
	$("#cb_oneTime").on('checked', function (event) {
		$("#schedule_days_select").jqxDropDownList('uncheckAll');		
	}); 
}

function renderSchedules() {	
	$("#schedules_container").html("");	
	if(lastResult.schedules.length > 0) {										
		for(k = 0; k < lastResult.schedules.length; k++) {
			var currentSchedule = lastResult.schedules[k];
			renderSchedule($("#schedules_container"), currentSchedule, false);
		}
	}	
}

function renderSchedule(container, schedule, editable) {
			
	var daysOfWeekHtml = "";
	
	for(j = 0; j < daysOfWeek.length; j++) {
		daysOfWeekHtml += `<div class="schedule_cb" id="cb_${daysOfWeek[j][0]}_${schedule.id}">${daysOfWeek[j][1]}</div>`;
	}
	var scheduleHtml = `
						<div class="schedule_header">
							${schedule['name']}
							<img src="del.png" class="schedule_delete" onclick="deleteSchedule(${schedule.id})"  alt="Delete schedule"/>
						</div>
						<div class="schedule_container">
							<div class="schedule_left">
								<div class="schedule_cb" id="cb_oneTime_${schedule.id}">One time</div>
								<div class="schedule_date" style="display:${schedule["oneTime"]?"block":"none"}">${(schedule.date < 10 ? '0':'') + schedule.date}/${(schedule.month < 10 ? '0' : '') + schedule.month}/${schedule.year}</div>
							</div>
							<div class="schedule_center">
								${daysOfWeekHtml}
							</div>
							<div class="schedule_right">																				
							  Time & dur.<br />                    
							  ${(schedule.startHour < 10 ? "0" : "") + schedule.startHour}:${(schedule.startMinute < 10 ? "0" : "") + schedule.startMinute}
							  /
							  ${schedule.durationSeconds} sec								
							</div>							
						</div>
						`;	
				
	container.append(scheduleHtml);
	
	$("#cb_oneTime_" + schedule.id).jqxCheckBox({ width: 100, height: 25, checked: schedule["oneTime"], disabled: !editable});
	for(j = 0; j < daysOfWeek.length; j++) {						
		$("#cb_" + daysOfWeek[j][0] + "_" + schedule.id).jqxCheckBox({ width: 112, height: 25, checked: schedule[daysOfWeek[j][0]], disabled: !editable});						
	}
}

function deleteSchedule(id) {
	setApi = remote + "api?DELSCHEDULE=" + id;
	changed = true;
	monitorData();
}

function addSchedule() {		
	var oneTime = $("#cb_oneTime").jqxCheckBox('checked');
	var date = $("#schedule_date").jqxDateTimeInput('getDate');
	var name = $("#schedule_name").val();
	var fromTime = $("#schedule_from").jqxDateTimeInput('getDate');
	var durationSeconds = $("#schedule_duration").val();
	
	scheduleString = encodeURIComponent(name) + "," + oneTime + "," + date.getFullYear() + "," + (date.getMonth() + 1) + "," + date.getDate();				
	for(j = 0; j < daysOfWeek.length; j++) {								
		scheduleString += "," + daysOfWeek[j][2];
	}
	scheduleString += "," + fromTime.getHours() + ","  + fromTime.getMinutes() + "," + durationSeconds;	
	setApi = remote + 'api?ADDSCHEDULE=' + encodeURIComponent(scheduleString);	
	changed = true;
	monitorData();
}