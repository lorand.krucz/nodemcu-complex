var comfortLevels = ["Too Dry", "Very Comfortable", "Comfortable", "Tolerable", "Uncomfortable", "Quite Uncomfortable", "Very Uncomfortable", "Severely Uncomfortable"];

function enableTemp() {		
	$('#temp_container').show();
	$('#chart_container').show();
	
	$('#tempGauge').jqxGauge({
		min:-20,
		max:50,
		ranges: [{ startValue: -20, endValue: 0, style: { fill: '#4b48b6', stroke: '#4b48b6' }, startWidth: 1, endWidth: 3 },
		   { startValue: 0, endValue: 10, style: { fill: '#4bb648', stroke: '#4bb648' }, startWidth: 3, endWidth: 5 },
		   { startValue: 10, endValue: 25, style: { fill: '#fbd109', stroke: '#fbd109' }, startWidth: 5, endWidth: 7 },
		   { startValue: 25, endValue: 35, style: { fill: '#ff8000', stroke: '#ff8000' }, startWidth: 7, endWidth: 9 },
		   { startValue: 35, endValue: 50, style: { fill: '#e02629', stroke: '#e02629' }, startWidth: 9, endWidth: 11 }],
		labels: { visible: true, position: 'inside', interval: 10 },
		ticksMinor: { interval: 1, size: '5%' },
		ticksMajor: { interval: 10, size: '9%' },
		value: 0,
		colorScheme: 'scheme05',
		animationDuration: 1800,
		radius: 120,
		caption: { value: 'Temperature', offset: [0, 11] },
	});
	
	$('#tempGauge').on('valueChanging', function (e) {
		$('#tempGaugeValue').text(parseFloat(e.args.value).toFixed(1) + ' °C');
	});   
	
	$('#humidGauge').jqxGauge({
		min:0,
		max:100,
		ranges: [{ startValue: 0, endValue: 20, style: { fill: '#4b48b6', stroke: '#4b48b6' }, startWidth: 1, endWidth: 3 },
		   { startValue: 20, endValue: 40, style: { fill: '#4bb648', stroke: '#4bb648' }, startWidth: 3, endWidth: 5 },
		   { startValue: 40, endValue: 60, style: { fill: '#fbd109', stroke: '#fbd109' }, startWidth: 5, endWidth: 7 },
		   { startValue: 60, endValue: 80, style: { fill: '#ff8000', stroke: '#ff8000' }, startWidth: 7, endWidth: 9 },
		   { startValue: 80, endValue: 100, style: { fill: '#e02629', stroke: '#e02629' }, startWidth: 9, endWidth: 11 }],
		labels: { visible: true, position: 'inside', interval: 10 },
		ticksMinor: { interval: 1, size: '5%' },
		ticksMajor: { interval: 10, size: '9%' },
		value: 0,
		colorScheme: 'scheme05',
		animationDuration: 1800,
		radius: 120,
		caption: { value: 'Relative Humidity', offset: [0, 11] },
	});
	$('#humidGauge').on('valueChanging', function (e) {
		$('#humidGaugeValue').text(parseFloat(e.args.value).toFixed(1) + ' %');
	}); 
	
	$('#heatIndexGauge').jqxGauge({
		min:-20,
		max:40,                   

		ticksMinor: { interval: 5, size: '5%' },
		ticksMajor: { interval: 10, size: '9%' },
		value: 0,
		colorScheme: 'scheme04',
		style: { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' },
		animationDuration: 1800,
		radius: 100,
		width:110,
		startAngle: 0,
		endAngle: 90,         

		showRanges:false,
		border:{ visible: false },
		labels: { 
			visible: true, 
			position: 'outside', 
			interval: 10,
			distance: '50px',
			offset: [-6, -11]
		},
	  
		style: { fill: '#00000000', stroke: '#00000000' },
		cap: { radius: 0.04 },
		caption: { value: 'Heat Index', position: 'left', offset: [20, 10] },
	});
	
	$('#heatIndexGauge').on('valueChanging', function (e) {
		$('#heatIndexGaugeValue').text(parseFloat(e.args.value).toFixed(1) + ' °C');
	});
			
	$('#dewPointGauge').jqxGauge({
		min:-20,
		max:40,                   

		ticksMinor: { interval: 5, size: '5%' },
		ticksMajor: { interval: 10, size: '9%' },
		value: 0,
		colorScheme: 'scheme04',
		style: { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' },
		animationDuration: 1800,
		radius: 100,
		width:110,
		startAngle: 0,
		endAngle: 90,         

		showRanges:false,
		border:{ visible: false },
		labels: { 
			visible: true, 
			position: 'outside', 
			interval: 10,
			distance: '50px',
			offset: [-6, -11]
		},
		style: { fill: '#00000000', stroke: '#00000000' },
		cap: { radius: 0.04 },
		caption: { value: 'Dew Point', position: 'left', offset: [20, 10] },
	});
	
	$('#dewPointGauge').on('valueChanging', function (e) {
		$('#dewPointGaugeValue').text(parseFloat(e.args.value).toFixed(1) + ' °C');
	});
	
	var settings = {
		title: "Recent History",
		description: "Temp / Humid fluctuations",
		enableAnimations: true,
		animationDuration: 1000,
		enableAxisTextAnimation: true,
		showLegend: true,
		padding: { left: 15, top: 5, right: 20, bottom: 5 },
		titlePadding: { left: 10, top: 0, right: 0, bottom: 10 },
		source: lastTempLog,
		xAxis:
		{
			dataField: 'timestamp',
			displayText: 'At',			
			valuesOnTicks: true,
			formatFunction: function (value) {       										
				return getAbsoluteTimestamp(value);
			},
		},
		colorScheme: 'scheme01',
		seriesGroups:
		[
		  {
			type: 'splinearea',
			alignEndPointsWithIntervals: true,
			valueAxis:
			{
			  title: {text: 'Humidity'},
			  position: 'right',
			  unitInterval: 20,
			  minValue: 0,
			  maxValue: 100,
			  labels: {formatSettings: { decimalPlaces: 2}},                                
			},
			series: [
				{ dataField: 'humid', displayText: 'Humidity', opacity: 1.0 }
			  ]
		  }, 
		  {                
			type: 'splinearea',
			alignEndPointsWithIntervals: true,
			valueAxis:
			{
			  visible: true,
			  unitInterval: 5,
			  title: {text: 'Temperature'},
			  labels: {
				horizontalAlignment: 'right',
				formatSettings: {decimalPlaces: 0}
			  }
			},
			series: [
				{ dataField: 'temp', displayText: 'Temperature ', opacity: 0.6 },                                    
			  ]
		  }		  
		]
	};

	$('#temp_humid_chart').jqxChart(settings);
}

function renderTemp() {
	$('#tempGauge').jqxGauge('value', lastResult.temperature);
	$('#humidGauge').jqxGauge('value', lastResult.humidity);
	$('#heatIndexGauge').jqxGauge('value', lastResult.heatindex);
	$('#dewPointGauge').jqxGauge('value', lastResult.dewpoint);               
	$('#comfortlevel').html(comfortLevels[lastResult.comfortlevel]);

	$("#comfort_hot").css({ opacity: lastResult.toohot ? 1 : 0.3 });
	$("#comfort_cold").css({ opacity: lastResult.toocold ? 1 : 0.3 });
	$("#comfort_dry").css({ opacity: lastResult.toodry ? 1 : 0.3 });
	$("#comfort_wet").css({ opacity: lastResult.toohumid ? 1 : 0.3 });
}

function renderTempLog() {			
	$('#temp_humid_chart').jqxChart('update');
}	