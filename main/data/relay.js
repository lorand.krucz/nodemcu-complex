function enableRelay() {
	$('#relays_container').show();
	$('#relays_chart_container').show();	
	
	var seriesGroups = [];	
	for(i = 0; i < skills['relays'].length; i++) {
		seriesGroups[i] = {                
			type: 'spline',
			alignEndPointsWithIntervals: true,
			valueAxis:
			{
			  visible: false,
			  unitInterval: 1,
			  title: {text: 'Relay Active'},
			  maxValue: 2,
			  labels: {
				horizontalAlignment: 'right',
				formatSettings: {decimalPlaces: 0}
			  }
			},
			series: [
				{ dataField: 'relay_' + skills.relays[i].pin, displayText: skills.relays[i].name, opacity: 0.6 },                                    
			  ]
			};
	}
	
	$('#relays_chart').jqxChart({
		title: "Recent History",
		description: "Relay state changes",
		enableAnimations: true,
		animationDuration: 1000,
		enableAxisTextAnimation: true,
		showLegend: true,
		padding: { left: 15, top: 5, right: 20, bottom: 5 },
		titlePadding: { left: 10, top: 0, right: 0, bottom: 10 },
		source: lastRelayLog,
		xAxis:
		{
			dataField: 'timestamp',
			displayText: 'At',			
			valuesOnTicks: true,
			formatFunction: function (value) {       										
				return getAbsoluteTimestamp(value);
			},
		},
		colorScheme: 'scheme05',
		seriesGroups: seriesGroups		
	});	
}

function renderRelays(renderResult) {		
	for(i = 0; i < renderResult.relays.length; i++) {
		var relay = renderResult.relays[i];
		var relayId = relay['pin'];
		var relayMqtt = decodeURIComponent(relay['mqtt']);
		
		var schedules = {};	
		for(j = 0; j < renderResult.schedules.length; j++) {
			schedules[j] = renderResult.schedules[j]["name"];
		}		
		
		if(!$('#relay_container_' + relayId).length) {
			$('#relays_container').append(
				`<div class="relay_container" id="relay_container_${relayId}">
					<div id="relay_name_${relayId}" class="relay_title"></div>
					<div class="relay_main">
						<div class="relay_cell_head">Switch</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='relay_${relayId}'>
							</div>
						</div>
					</div>
					<div class="relay_schedules">
						<div class="relay_cell_head">Schedules</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='relay_schedule_select_${relayId}'></div>
						</div>
					</div>					
					<div class="relay_schedule">
						<div class="relay_cell_head">Sched.</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='schedule_${relayId}'></div>							
						</div>
					</div>
					<div class="relay_pir">
						<div class="relay_cell_head">Pir</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='pir_${relayId}'></div>
						</div>
					</div>					
					<div class="relay_heat">
						<div class="relay_cell_head">Heat</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='heat_${relayId}'></div>
						</div>
					</div>
					<div class="relay_cool">
						<div class="relay_cell_head">Cool</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<div id='cool_${relayId}'></div>
						</div>
					</div>
					<div class="relay_target">
						<div class="relay_cell_head">Target temperature</div>
						<div class="relay_cell_body">
							<div id="tempSlider_${relayId}"></div>
						</div>
					</div>
					<div class="relay_impulse">
						<div class="relay_cell_head">Impulse</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<input style="float:left" type="number" value="500" min="100" max="10000" id="impulseVal_${relayId}" />							
							<input type="button" value="Trig." style="float:left;margin-left:5px" id="impulseTrig_${relayId}" />							
						</div>
					</div>
					<div class="relay_mqtt">
						<div class="relay_cell_head">Mqtt</div>
						<div class="relay_cell_body relay_cell_body_padded">
							<img src="console.png" id="mqtt_console_button_${relayId}" class="console_button" alt="Edit mqtt reactions"/>
							<div id="mqtt_console_${relayId}" class="console_content">
								<div>								
									Mqtt Console Window
								</div>
								<div>
									<div>
										Add mqtt commands below as [channel][space][value][newline]
										You may use @@on@@ as a template value to be replaced by "true" or "false" according to the relay state.																				
									</div>	
									<textarea id="mqtt_console_content_${relayId}">${relayMqtt}</textarea>
									<div style="float: right; margin-top: 15px;">
										<input type="button" id="save_mqtt_${relayId}" value="SAVE" style="margin-right: 10px" />										
									</div>								
								</div>
							</div>
						</div>
					</div>
				</div>
				`);			
			$('#relay_' + relayId).jqxSwitchButton({ height: 22, width: 55,  theme: 'office' });			
			$('#pir_' + relayId).jqxSwitchButton({ height: 22, width: 45,  theme: 'default' });			
			$('#schedule_' + relayId).jqxSwitchButton({ height: 22, width: 45,  theme: 'default' });
			$('#heat_' + relayId).jqxSwitchButton({ height: 22, width: 45,  theme: 'orange' });
			$('#cool_' + relayId).jqxSwitchButton({ height: 22, width: 45,  theme: 'energyblue' });			
			$('#tempSlider_' + relayId).jqxSlider({ tickSize: 10, width: 255, height: 40, mode: "fixed", tooltip: true, showButtons: false, ticksFrequency: 5, min: -5, max: 40, showMinorTicks: true, minorTicksFrequency: 1, showTickLabels: true });			
			$("#relay_schedule_select_" + relayId).jqxDropDownList({ checkboxes: true, height:25, width: 235, source: schedules});
			$("#impulseVal_" + relayId).jqxNumberInput({ width: 90, height: 26, inputMode: 'simple',  decimalDigits: 0, symbol: ' ms', symbolPosition: 'right', spinButtons: true, spinButtonsStep: 100});
			$("#impulseTrig_" + relayId).jqxButton({ width: 45, height: 26 });
			
						
			$('#relay_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",on,"));
			$('#pir_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",pir,"));			
			$('#schedule_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",schedule,"));
			$('#heat_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",heat,"));
			$('#cool_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",cool,"));
			$('#tempSlider_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",temptarget,"));
			$('#impulseTrig_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",impulse,"));
			$('#impulseTrig_' + relayId).attr("relayId", relayId);
			$('#impulseTrig_' + relayId).attr("relayId", relayId);
			$('#mqtt_console_button_' + relayId).attr("relayId", relayId);
			$('#mqtt_console_button_' + relayId).attr("actionurl", "SETRELAY=" + encodeURIComponent(relayId + ",mqtt,"));
			
			$('#relay_schedule_select_' + relayId).attr("activateurl", "ADDRELAYSCHEDULE=" + relayId + ",");			
			$('#relay_schedule_select_' + relayId).attr("deactivateurl", "DELRELAYSCHEDULE=" + relayId + ",");			
	
			var switchSetFunction = function(event) {			
				setRelayData($(event.target).attr("actionurl") + !event.args.checked);
			}
						
			$("#relay_" + relayId).on('change', switchSetFunction);
			$('#pir_' + relayId).on('change', switchSetFunction);			
			$('#schedule_' + relayId).on('change', switchSetFunction);
			$('#heat_' + relayId).on('change', switchSetFunction);
			$('#cool_' + relayId).on('change', switchSetFunction);
			
			$('#tempSlider_' + relayId).on('slideStart', function(event) {
				$(event.target).attr("busy", true);				
			});
			$('#tempSlider_' + relayId).on('slideEnd', function(event) {				
				$(event.target).attr("busy", false);				
				setRelayData($(event.target).attr("actionurl") + event.args.value);				
			});		
			$("#relay_schedule_select_" + relayId).on('checkChange', function (event) {
				if (event.args) {					
					var item = event.args.item;					
					if(item.checked) {
						setRelayData($(event.target).attr("activateurl") + item.value);
					} else {
						setRelayData($(event.target).attr("deactivateurl") + item.value);
					}
				}
			});	
			
			$("#impulseTrig_" + relayId).on('click', function(event) {				
				setRelayData($(event.target).attr("actionurl") + $('#impulseVal_' + $(event.target).attr("relayId")).val());				
			});			
			
			$("#mqtt_console_button_" + relayId).on('click', function(event) {	
				var relayId = $(event.target).attr("relayId");
				var actionUrl = $(event.target).attr("actionurl");
				
				$("#mqtt_console_" + relayId).jqxWindow({
					position: { x: $("#mqtt_console_button_" + relayId).offset().left - 470, y: $("#mqtt_console_button_" + relayId).offset().top + 30} , 
					height: 260, width: 500, isModal: true, theme: 'energyblue',
					okButton: $('#save_mqtt_' + relayId)
				});
				$("#mqtt_console_content_" + relayId).jqxTextArea({ placeHolder: 'abcdef456789 SETRELAY=12,on,@@on@@', height: 120, width: 480});
			
				$('#mqtt_console_' + relayId).jqxWindow('open');
				
				$('#mqtt_console_' + relayId).on('close', function (event) {
					if (event.type === 'close' && event.args.dialogResult.OK) {										
						setRelayData(actionUrl + encodeURIComponent($("#mqtt_console_content_" + relayId).val()));
					}
				});
			});	
		}
		
		if(!skills.dht) {
			$('#heat_' + relayId).hide();
			$('#cool_' + relayId).hide();
			$('#tempSlider_' + relayId).hide();
		} else {
			$('#heat_' + relayId).show();
			$('#cool_' + relayId).show();
			$('#tempSlider_' + relayId).show();			
		}
		
		if(skills.pir) {
			$('#pir_' + relayId).show();
		} else {
			$('#pir_' + relayId).hide();			
		}
		
		$('#relay_name_' + relayId).html("[" + relay['pin'] + "] " + relay['name']);
		if($('#relay_' + relayId).jqxSwitchButton('checked') != relay["on"]) {
			$('#relay_' + relayId).jqxSwitchButton({ checked:relay["on"] });
		}
		if($('#pir_' + relayId).jqxSwitchButton('checked') != relay["pir"]) {
			$('#pir_' + relayId).jqxSwitchButton({ checked:relay["pir"] });
		}				
		if($('#schedule_' + relayId).jqxSwitchButton('checked') != relay["schedule"]) {
			$('#schedule_' + relayId).jqxSwitchButton({ checked:relay["schedule"] });
		}
		if($('#heat_' + relayId).jqxSwitchButton('checked') != relay["heat"]) {
			$('#heat_' + relayId).jqxSwitchButton({ checked:relay["heat"] });
		}
		if($('#cool_' + relayId).jqxSwitchButton('checked') != relay["cool"]) {
			$('#cool_' + relayId).jqxSwitchButton({ checked:relay["cool"] });
		}		
		if(!$('#tempSlider_' + relayId).attr("busy") && $('#tempSlider_' + relayId).jqxSlider('value') != relay["temptarget"]) {
			$('#tempSlider_' + relayId).jqxSlider({ value: relay["temptarget"] });
		}		
		
		if(JSON.stringify(schedules) != JSON.stringify($("#relay_schedule_select_" + relayId).jqxDropDownList("source"))) {
			$("#relay_schedule_select_" + relayId).jqxDropDownList({source: schedules});
		}	

		$("#impulseTrig_" + relayId).jqxButton({theme: relay["impulse"] ? "orange":"default" });

		var items = $("#relay_schedule_select_" + relayId).jqxDropDownList('getCheckedItems'); 
		var currentSelectedIndexes = [];
		
		for(k = 0; k < items.length; k++) {				
			currentSelectedIndexes[k] = items[k].index;			
		}
				
		for(j = 0; j < renderResult.relayschedules.length; j++) {
			if(renderResult.relayschedules[j].relayId == relayId) {
				var scheduleIndex = renderResult.relayschedules[j].scheduleId;				
				if(currentSelectedIndexes.includes(scheduleIndex)) {
					currentSelectedIndexes.splice(currentSelectedIndexes.indexOf(scheduleIndex), 1);
				} else {
					$("#relay_schedule_select_" + relayId).jqxDropDownList('checkIndex', scheduleIndex);
				}
			}
		}		
		
		for(j = 0; j < currentSelectedIndexes.length; j++) {
			$("#relay_schedule_select_" + relayId).jqxDropDownList('uncheckIndex', currentSelectedIndexes[j]);
		}						
	}	
}

function setRelayData(params) {
	setApi = remote + "api?" + params;
	changed = true;
	monitorData();
}

function renderRelaysLog() {			
	$('#relays_chart').jqxChart('update');
}	
	