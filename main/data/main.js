var monitorDataRepeatTask;
var monitorLogRepeatTask;
var ajaxTimeoutMs = 10000;

function monitorData() {
	clearInterval(monitorDataRepeatTask);
	getData();
	monitorDataRepeatTask = setInterval(getData, 5000);
}

function monitorLog() {
	clearInterval(monitorLogRepeatTask);
	if(skills.dht || skills.relay) {
		getLog();
		monitorLogRepeatTask = setInterval(getLog, 120000);
	}
}		

function getSkills() {
	var clearChanged = changed;	
	$("#reddot").css({ opacity: 1 });
	$('#error').html("");	
	$.ajax({		
		url: remote + "skills", 
		timeout: ajaxTimeoutMs,		
		success: function(result){  		
			console.log(result);
			skills = result;			
			enableComponents();				
		},        
		error: function(result){					
			$('#error').html("A network error occured while getting skills");					
		},
		complete: function(result){								
			$("#reddot").css({ opacity: 0.3 });       
		}	
	});	
}

function getData() {	
	var clearChanged = changed;	
	$("#reddot").css({ opacity: 1 });
	$('#error').html("");
			
	$.ajax({
		url: changed ? setApi : (remote + "api"), 
		timeout: ajaxTimeoutMs,
		success: function(result){  
			if(clearChanged) changed = false;
			if(changed) return;			
			
			result.schedulesChanged = lastResult == null ||	(lastResult.schedules + "" != result.schedules + "");	
			lastResult = result;
			
			renderResult();							
		},        
		error: function(result){			
			$('#error').html("A network error occured while getting data");					
		},
		complete: function(result){								
			console.log("complete");
			$("#reddot").css({ opacity: 0.3 });       
		}	
	});	
}

function getLog() {
	if(skills.dht || skills.relay) {
		$("#reddot").css({ opacity: 1 });
		$('#error').html("");	
			
		$.ajax({
			url: remote + "fileLog2.txt", 
			timeout: ajaxTimeoutMs,
			success: function(result2) {	
				$.ajax({
					url: remote + "fileLog1.txt", 
					timeout: ajaxTimeoutMs,
					success: function(result1) {	
						result = result2 + result1;
						resultLines = result.split(/\r?\n/);
						
						tempLogCount = 0;
						relayLogCount = 0;
						
						for(i = 0; i < resultLines.length; i++) {
							resultLineTokens = resultLines[i].split(",");								
							
							if(resultLineTokens[0] == 1) {
								lastTempLog[tempLogCount] = {timestamp: resultLineTokens[1], temp : resultLineTokens[2] / 10, humid : resultLineTokens[3] / 10};	
								tempLogCount++;
							}
							if(resultLineTokens[0] == 2) {								
								lastRelayLog[relayLogCount] = {timestamp: resultLineTokens[1] - 1};				
								for(j = 2; j < resultLineTokens.length; j++) {
									relayAndState = resultLineTokens[j].split(":");
									lastRelayLog[relayLogCount]['relay_' + relayAndState[0]] = (parseInt(relayAndState[1]) + 1) % 2;
								}
								relayLogCount++;
								
								lastRelayLog[relayLogCount] = {timestamp: parseInt(resultLineTokens[1])};
								for(j = 2; j < resultLineTokens.length; j++) {
									relayAndState = resultLineTokens[j].split(":");
									lastRelayLog[relayLogCount]['relay_' + relayAndState[0]] = parseInt(relayAndState[1]);
								}
								relayLogCount++;
							}							
						}						
						if(tempLogCount > 0) {
							renderTempLog();
						}						
						if(relayLogCount > 0) {
							lastRelayLog[relayLogCount] = Object.assign({}, lastRelayLog[relayLogCount - 1]);;
							lastRelayLog[relayLogCount]['timestamp'] = parseInt(Date.now() / 1000);
							renderRelaysLog();
							console.log(lastRelayLog);
						}	
					},
					error: function(result1) {
					$('#error').html("A network error occured while getting history");
					},
					complete: function(result1){
					$("#reddot").css({ opacity: 0.3 });
					}
				});		
			},
			error: function(result2) {
			$('#error').html("A network error occured while getting history");
			},
			complete: function(result2){
			$("#reddot").css({ opacity: 0.3 });
			}
		});
	}	
}

function renderHeader() {
	$('#identity').html(lastResult.identity + " [" + lastResult.uuid + "]");
	$('#memgage').jqxGauge('value', (lastResult.freemem / 1000));	
	$('#wifigage').jqxGauge('value', (lastResult.wifirssi));
	$('#memgageValue').html(Math.round(lastResult.freemem / 1000));
	$('#wifigageValue').html(lastResult.wifirssi);
	switch(lastResult.mqttstate) {
		case 0: 
			$('#mqttstate').css({"color": "lightgray"});
			$('#mqttstate').html("MQTT off");
			break;
		case 1: 
			$('#mqttstate').css({"color": "magenta"});
			$('#mqttstate').html("MQTT disconnected");
			break;
		case 2: 
			$('#mqttstate').css({"color": "green"});
			$('#mqttstate').html("MQTT connected");
			break;
	}			
	$('#clock').html(lastResult.datetime);
	
	var automataConsoleContent = "";	
	for(i = 0; i < lastResult.automata.length; i++) {
		automataConsoleContent += lastResult.automata[i].timeOffset + " " + lastResult.automata[i].multiInst + "\r\n";
	}		
	$('#automata_console_text').val(automataConsoleContent);		
}

function getRelativeTimestamp(offsetMinutes) {	
	var date = new Date(lastResult.datetime);		
	
	date.setMinutes(date.getMinutes() - offsetMinutes);
	var h = date.getHours();
	var m = date.getMinutes();
	return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);	
}

function getAbsoluteTimestamp(timestamp) {	
	var date = new Date(timestamp * 1000);		
		
	var h = date.getHours();
	var m = date.getMinutes();
	if(isToday(date)) {
		return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);	
	} else {
		var d = date.getDate();
		var M = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()];
		var Y = (date.getFullYear() + "").slice(-2);		
		return d + "." + M + "." + Y + " " +(h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m);	
	}
}

const isToday = (someDate) => {
  const today = new Date()
  return someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
}