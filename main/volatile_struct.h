struct TempHumidData {
  boolean senzorError;
  
  float temperature;
  float heatindex;
  float humidity;
  float dewpoint;
  byte comfortlevel;
  
  boolean toohot;
  boolean toocold;
  boolean toohumid;
  boolean toodry;
};

struct DateTimeData {
  int year;
  int month;
  int date;
  int weekday;
  int hour;
  int minute;
  int second;
  unsigned timestamp;
};
