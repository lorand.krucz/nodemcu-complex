int maxHeap = ESP.getFreeHeap();

#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include "uTimerLib.h"
#include "DHTesp.h"

#include "persistent_op.h"
#include "volatile_op.h"
#include "httproutines.h"
#include "responses.h"
#include "sys_utils.h"
#include "mqtt.h"
#include "ota.h"

const int NAME_MAXLEN = 40;
const char NAME[NAME_MAXLEN] = "NodeMCU Complex V7.1";

#define BUTTON 0
#define LED 2

#define SERIAL_FREQ 9600
#define EEPROM_SIZE 2500

#define MQTT_OFF 0
#define MQTT_DISCONNECTED 1
#define MQTT_CONNECTED 2

#define HEART_BEAT_FREQ_S 10
#define PERSIST_LOG_FREQ_HB 60          //every 10 minutes
#define MQTT_REPORT_API_FREQ_HB 12      //every 2 minutes
#define MQTT_RECONNECT_TIMEOUT_MS 20000 //every 20 seconds

#define MAX_SCHEDULES 10
#define HOUR_IN_SEC 3600
#define DEG_BUFFER 0.25

#define BUZZER_FREQ_HZ 500

#define WDT_TIME 6000

#define DHTTYPE DHTesp::DHT22

String ssid = "NodeMCU_" + getDeviceUID();

boolean configMode = false;
boolean configChanged = false;
boolean anyRelayHasChanged = false;
int hbCounter;
boolean wasMotionPresent;
boolean motionPresent;
unsigned long lastMqttReconnectAttempt = 0;
unsigned long lastRefreshState = 0;
boolean heartBeat = false;
boolean reboot = false;
int mqttstate = MQTT_OFF;

IPAddress softApIp(192, 168, 0, 1);
IPAddress softApSn(255, 255, 255, 0);

AsyncWebServer *server = new AsyncWebServer(80);
DHTesp *dht = new DHTesp();
WiFiClient wifiClient;
PubSubClient *mqttClient = new PubSubClient(wifiClient);

//This method will react to motion sensor interrups (must be in IRAM or it will crash, hence the ICACHE_RAM_ATTR)
void ICACHE_RAM_ATTR motionSense() {    
  if(storedConfig.pirpin != -1) {
    motionPresent = digitalRead(storedConfig.pirpin);  
  }
}
//This method will react to timer interrups (must be in IRAM or it will crash, hence the ICACHE_RAM_ATTR)
void ICACHE_RAM_ATTR heartBeatSense() {    
  heartBeat = true;
}

//Common setup steps 
void setup(void) {    
  ESP.wdtEnable(WDT_TIME);
  //**************************************************************************initialize the serial, the blinking led pin, the eeprom for settings and the flash file system
  Serial.begin(SERIAL_FREQ);      
  Serial.println("Initial setup...");  
  pinMode(LED, OUTPUT);
  EEPROM.begin(EEPROM_SIZE);
  LittleFS.begin();  

  LittleFS.open("/fileLog1.txt", "a+").close();
  LittleFS.open("/fileLog2.txt", "a+").close();
    
  //**************************************************************************read the stored name and compare to what we expect it to be (this is essentially the DB version control)
  String readName = loadStringFromEeprom(0, NAME_MAXLEN);    
  if(readName != String(NAME)) {   
    //**************************************************************************if version bad, save blank configuration  
    Serial.println(String(NAME) + " != " + readName);
    saveStringToEeprom(0, NAME);      
    Serial.println("Saving blank config...");
    saveConfigToEeprom(NAME_MAXLEN + 1);
  } else {
    //**************************************************************************if version good, load the saved configuration 
    Serial.println("Loading existing config...");
    loadConfigFromEeprom(NAME_MAXLEN + 1);        
  }

  if (strlen(storedConfig.ssid) == 0 && !storedConfig.allwaysap) {
    //**************************************************************************if there is not WIFI configured we want to go straight to Acces Point mode to configure the device
    Serial.println("No SSID stored, and not always AP, going to config...");
    configMode = true;
  } 
  
  //**************************************************************************start device in the required mode config vs normal
  if (configMode) {
    configSetup();
  } else {
    normalSetup();
  }
}

void wifiSetup() {
  Serial.println("Setting up wifi...");

  WiFi.softAPdisconnect(true);
  WiFi.disconnect(true);  
  
  if(configMode || storedConfig.allwaysap) {
    //start AP with random channel
    randomSeed(millis());
    int wifiChannel = random(1,13);
    WiFi.softAPConfig(softApIp, softApIp, softApSn);
    if(WiFi.softAP(ssid.c_str(),"NodeMCU1",wifiChannel,false,4)) {
      Serial.println("Started AP " + ssid + ",  channel:" + String(wifiChannel) + ", IP: " +  WiFi.softAPIP().toString());                
    } else {
      Serial.println("Failed starting AP " + ssid);    
    }       
  } 
  if(!configMode && strlen(storedConfig.ssid) != 0){    
    IPAddress ipAddr;
    IPAddress gatewayAddr;
    IPAddress netmaskAddr;
    IPAddress dnsAddr;
    if (ipAddr.fromString(storedConfig.ip) && gatewayAddr.fromString(storedConfig.gateway) &&
        netmaskAddr.fromString(storedConfig.netmask) && dnsAddr.fromString(storedConfig.dns)) {
      WiFi.config(ipAddr, gatewayAddr, netmaskAddr, dnsAddr);
    }
    WiFi.begin(storedConfig.ssid, storedConfig.pass);
    
    int result = WiFi.waitForConnectResult();
    switch(result) {
      case WL_CONNECTED:
        Serial.println("Connected to: " + String(storedConfig.ssid) + " IP: " +  WiFi.localIP().toString());
        break;
      case WL_NO_SSID_AVAIL:
        Serial.println("Network not available: " + String(storedConfig.ssid));
        break;
      case WL_CONNECT_FAILED:
        Serial.println("Password incorect for: " + String(storedConfig.ssid));
        break;
      default: 
        Serial.println("Connection failed to " + String(storedConfig.ssid));
    }    
  }  
    
  int wifiMode = WiFi.getMode();
  switch(wifiMode) {
      case WIFI_AP:
        Serial.println("AP mode");
        break;
      case WIFI_STA:
        Serial.println("STA mode");
        break;
      case WIFI_AP_STA:
        Serial.println("AP and STA mode");
        break;
      case WIFI_OFF: 
        Serial.println("Wifi OFF");
    }  

    configOta();
}

//Set up the device to be a Wifi Access Point - for initial configuration
void configSetup() {
  Serial.println("Entering config state...");

  forceConfig();

  wifiSetup(); 

  //**************************************************************************run limited server that allows only config  
  server->on("/", [] (AsyncWebServerRequest *request) { Serial.println("Getting file: /config.html"); hanldeFileRequest(request, "/config.html"); });
  server->on("/configapi", handleConfigApi);
  server->onNotFound(hanldeDefaultRequest);

  server->begin();
}

void forceConfig() {
  Serial.println("Write \"reconfig\" or press button to restart in configuration mode");
  Serial.println("Write \"forceconfig=SSID PWD\" to force connect to a different wifi");
  pinMode(BUTTON, INPUT_PULLUP);

  for (int i = 0; i < 6; i++) {
    Serial.print(".....");       
    delay(20);
    digitalWrite(LED, LOW);
    delay(20);
    digitalWrite(LED, HIGH);

    String commandLine = Serial.readString();
    commandLine.trim();    
    
    String command = split(commandLine, '=', 0);
    String value = split(commandLine, '=', 1);
    
    if(command == "reconfig" || (digitalRead(BUTTON) == LOW)) {
      Serial.println("Restarting in configuration mode ...");
      clearWifiData();
      saveConfigToEeprom(NAME_MAXLEN + 1);
      ESP.restart();
    }   
    
    if(command == "forceconfig") {
      Serial.println("Forcing configuration ...");      
    
      String ssid = split(value, ' ', 0);
      String pass = split(value, ' ', 1);
      forceWifiData(ssid, pass);
      saveConfigToEeprom(NAME_MAXLEN + 1);
      ESP.restart();
    }
  }
}

//Set up the device to be a Wifi Client - for normal execution
void normalSetup() {
  Serial.println("Entering normal state...");  
  
  forceConfig();
    
  wifiSetup();
    
  //**************************************************************************configure system timezone if we have network start up Network Time Protocol 

  setenv("TZ", storedConfig.timezone, 1);
  tzset();
  configTime(0, 0, "ro.pool.ntp.org", "pool.ntp.org", "time.nist.gov");

  //**************************************************************************configure IO pins for various sensors and actuators as needed
    
  if(storedConfig.dhtpin != -1) {
    dht->setup(storedConfig.dhtpin, DHTTYPE);
  }

  if(storedConfig.piezopin != -1) {
    analogWrite(storedConfig.piezopin, 0);
    analogWriteFreq(BUZZER_FREQ_HZ);
  }

  if(storedConfig.pirpin != -1) {
    pinMode(storedConfig.pirpin, INPUT);    
  }
  
  if(storedConfig.steppersteppin != -1) {
      pinMode(storedConfig.steppersteppin, OUTPUT);    
  }
  if(storedConfig.stepperdirpin != -1) {
      pinMode(storedConfig.stepperdirpin, OUTPUT);      
  }
  if(storedConfig.stepperenablepin != -1) {
      pinMode(storedConfig.stepperenablepin, OUTPUT);      
  }   
      
  //**************************************************************************start heart beat for data update and logging and stuff  
  TimerLib.setInterval_s(heartBeatSense, HEART_BEAT_FREQ_S);   
  
  //**************************************************************************run full server that allows all  
  server->on("/", [] (AsyncWebServerRequest *request) { hanldeFileRequest(request, "/index.html"); });
  server->on("/config", [] (AsyncWebServerRequest *request) { hanldeFileRequest(request, "/config.html"); });
  server->on("/skills", handleSkills);  
  server->on("/api", handleApi);    
  server->on("/configapi", handleConfigApi);
  server->onNotFound(hanldeDefaultRequest);
  server->begin();    
  
  Serial.println("Server started...");
  
  //**************************************************************************establish MQTT connection and send initial state
  
  mqttEstablish();  
}

bool mqttConfigured() {
  return strlen(storedConfig.ssid) > 0 && strlen(storedConfig.mqtturl) > 0 && storedConfig.mqttport != -1;
}

void mqttEstablish() {  
  long now = millis();
  if(mqttConfigured() && !mqttClient->connected() &&  WiFi.isConnected()) {    
    if (now - lastMqttReconnectAttempt > MQTT_RECONNECT_TIMEOUT_MS) {
      lastMqttReconnectAttempt = now;
      // Attempt to reconnect
      if(mqttConnect(mqttClient, storedConfig.mqtturl, storedConfig.mqttport, storedConfig.identity, getDeviceUID(), storedConfig.mqttuser, storedConfig.mqttpass, storedConfig.mqttbasepath, storedConfig.mqttcmdchannel, mqttCallback)) {
        lastMqttReconnectAttempt = 0;
        Serial.println("Mqtt Connected");
        mqttReport(true, true, false);
      } else {
        Serial.println("Mqtt Connect failed");
      }
    }
  }    
}

void mqttReport(bool skills, bool api, bool log) {
  ESP.wdtDisable();
  if(skills) {
    mqttSend(mqttClient, storedConfig.mqttbasepath, getDeviceUID() + "_skills", getSkillsResponse(), true);
  }
  if(api) {
    mqttSend(mqttClient, storedConfig.mqttbasepath, getDeviceUID() + "_api", getApiResponse(), false);
  }    
  if(log) {
    //todo
  }
  
  ESP.wdtEnable(WDT_TIME);
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {  
  String payloadStr = bufferToString(payload, length); 
  Serial.println("Mqtt payload: " + payloadStr);
  
  if(String(storedConfig.mqttcmdjson) != "") {
    StaticJsonDocument<128> jsonInBuffer;    
    deserializeJson(jsonInBuffer, payload);
    String message = jsonInBuffer["data"];  
    executeMultiInstruction(message);  
  } else {
    String message = bufferToString(payload, length);    
    executeMultiInstruction(message);  
  }
}

void executeMultiInstruction(String message) {   
  int count = 0;
  String instruction;  

  Serial.println("Mqtt instruction: " + message);
  
  while((instruction = split(message, '&', count)) != "") {
    String name = split(instruction, '=', 0);
    String value = split(instruction, '=', 1);
    value.trim();
    
    configChanged |= executeInstruction(name, value);    
        
    count++;        
  }

  if(configChanged) {
    readEnv();
    saveConfigToEeprom(NAME_MAXLEN + 1);    
  } 
}

//handler function for configuration api call
void handleConfigApi(AsyncWebServerRequest *request) {
  Serial.println("Getting config api...");

  //**************************************************************************handle any incoming configuration instructions
  if (request->args() > 0) {
    for (int i = 0; i < request->args(); i++) {
      String name = request->argName(i);
      String value = request->arg(i);
      if(pullConfigFromInstruction(name, value)) {
        saveConfigToEeprom(NAME_MAXLEN + 1);
      } else if (name == "REBOOT" && value == "true") {                
        reboot = true;        
      }      
    }    
  }
  
  //**************************************************************************construct and send API response
  String s = API_resp_config;  
  s.replace("@@uuid@@", getDeviceUID());
  s = templateFillConfig(s);
  s.replace("@@wifis@@", getWifiJsonList());  
  s.replace("@@relays@@", templateFillRelayArray(API_resp_config_relay, "[", "]", ","));
    
  AsyncWebServerResponse *response = request->beginResponse(200, "application/json", s);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

//handler function for skills api call
void handleSkills(AsyncWebServerRequest *request) {    
  AsyncWebServerResponse *response = request->beginResponse(200, "application/json", getSkillsResponse());
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

String getSkillsResponse() {
  String s = SKILLS_resp;
  s.replace("@@uuid@@", getDeviceUID());
  s.replace("@@maxmem@@", String(maxHeap));
  
  s = templateFillConfig(s);    
  s.replace("@@relays@@", templateFillRelayArray(API_resp_config_relay, "[", "]", ","));
  return s;
}

//handler function for main api call
void handleApi(AsyncWebServerRequest *request) {
  Serial.println("Getting api...");

  //**************************************************************************handle any incoming instructions
  if (request->args() > 0) {    
    
    for (int i = 0; i < request->args(); i++) {
      String name = request->argName(i);
      String value = request->arg(i);
      value.trim();   
      configChanged |= executeInstruction(name, value);
    }

    if(configChanged) {
      readEnv();
      saveConfigToEeprom(NAME_MAXLEN + 1);      
    }    
  }

  //**************************************************************************construct and send API response     
  AsyncWebServerResponse *response = request->beginResponse(200, "application/json", getApiResponse());
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);  
}

boolean executeInstruction(String name, String value) {    
  if(name == "PIEZO") {
    if(storedConfig.piezopin != -1) {
      analogWrite(storedConfig.piezopin, value == "true" ? 512 : 0);
    }
  } else if(name == "MVSTP") {          
     unsigned long steps = strtoul(split(value, ',', 0).c_str(), NULL, 10);
     unsigned long startperiodUs = strtoul(split(value, ',', 1).c_str(), NULL, 10) * 100;
     unsigned long targetperiodUs = strtoul(split(value, ',', 2).c_str(), NULL, 10) * 100;
     unsigned long accgainUs = strtoul(split(value, ',', 3).c_str(), NULL, 10);
     bool forward = split(value, ',', 4) == "true";
     moveStepper(steps, startperiodUs, targetperiodUs, accgainUs, forward);  
  } else if(name == "DISSTP") {     
    if(storedConfig.stepperenablepin != -1) {      
      digitalWrite(storedConfig.stepperenablepin, HIGH);
    }  
  } else if(name == "MQTTREPORT") {       
    String logType = split(value, ',', 0);
    int relayId = -1;
    if(logType == "relaylog") {
       relayId = split(value, ',', 1).toInt();
    }       
    mqttReport(logType == "skills" || logType == "all", logType == "api" || logType == "all", logType == "log" || logType == "all");
  } else if(pullFromInstruction(name, value)) {
    refreshState();
    return true;
  } 
  
  return false;  
}

String getApiResponse() {
  String s = API_resp;   

  s.replace("@@uuid@@", getDeviceUID());
  s.replace("@@mqttstate@@", String(mqttstate));
  s.replace("@@freemem@@", String(ESP.getFreeHeap()));
  s.replace("@@wifirssi@@", String(WiFi.RSSI()));
  
  s = templateFillConfig(s);  
  s = templateFillEnvironment(s);  
  s.replace("@@automata@@", templateFillAutomataArray(API_resp_automata, "[", "]", ","));  
  s.replace("@@relays@@", templateFillRelayArray(API_resp_relay, "[", "]", ","));  
  s.replace("@@schedules@@", templateFillScheduleArray(API_resp_schedule, "[", "]", ","));    
  s.replace("@@relayschedules@@", templateFillRelayScheduleArray(API_resp_relay_schedule, "[", "]", ","));

  return s;
}

//heart beat function, called periodically to read sensors, update the date/time clock values and log data 
void executeHeartBeat() {
  //read sensors
  readEnv();
  //do a datalog if needed
  if(hbCounter % PERSIST_LOG_FREQ_HB == 0) {    
    persistLog(getTempHumidLogLine());
  }
  
  if(hbCounter % MQTT_REPORT_API_FREQ_HB == 0) {    
    mqttReport(false, true, false);
  }

  hbCounter++;  
  
  Serial.println("Heart beat...");

  heartBeat = false;   
}

void readEnv() {
  //read stuff: motion sensor, temp&humid sensor and clock  
  readSensor(dht);  
  readClock();  
}

//This methods configures the relays, scans through all relays and check for settings / conditions to turn them On/Off as needed
void refreshState() {  
  boolean hasPirRelay = false;
  unsigned long oldRefreshState = lastRefreshState;
  lastRefreshState = millis();
  
  if(storedConfig.relayCount > 0) {       
    //**************************************************************************scan through all relays 
    for(int i = 0; i < storedConfig.relayCount; i++) { 
      hasPirRelay |= storedRelays[i].pir;         
      boolean hasSchedule = false;
      boolean isActivePeriod = false;   
      boolean hasChanged = false;
      //**************************************************************************if relay is schedule bound, scan through all schedules to find ones that are bound to it
      if(storedRelays[i].schedule) {
        for(int j = 0; j < storedConfig.relayScheduleCount; j++) {
          if(storedRelaySchedules[j].relayId == storedRelays[i].pin) {
            hasSchedule = true;
            //**************************************************************************check if bound schedule is in active period
            isActivePeriod |= isScheduleActive(storedRelaySchedules[j].scheduleId);
            if(isActivePeriod) {
              break;
            }            
          }
        }      
      }            
      //**************************************************************************if relay is PIR bound, check if motion is present + schedule active period if applicable
      if(storedRelays[i].pir) {        
        storedRelays[i].on = (!(storedRelays[i].schedule ^ isActivePeriod)) && motionPresent;             
      //**************************************************************************if relay is HEAT or COOL bound, check the target temperature + schedule active period if applicable
      } else if(storedRelays[i].heat) {
        float target = ((float)storedRelays[i].temptarget) + (storedRelays[i].on ? 1 : -1) * DEG_BUFFER;        
        storedRelays[i].on = (!(storedRelays[i].schedule ^ isActivePeriod)) && (target > tempHumidData.temperature) && !tempHumidData.senzorError;
      } else if(storedRelays[i].cool) {
        float target = ((float)storedRelays[i].temptarget) + (storedRelays[i].on ? -1 : 1) * DEG_BUFFER;
        storedRelays[i].on = (!(storedRelays[i].schedule ^ isActivePeriod)) && (target < tempHumidData.temperature) && !tempHumidData.senzorError;
      //**************************************************************************if relay is not bound to other stuff apply schedule active period if applicable
      } else if(storedRelays[i].schedule) {          
        storedRelays[i].on = hasSchedule && isActivePeriod;        
      }

      //**************************************************************************Set the relay to the state required
      if(storedRelays[i].impulse == 0) {
        hasChanged |= writeToRelay(storedRelays[i].pin, storedRelays[i].on);
      } else {        
        if(storedRelays[i].impulse > 0) {
          hasChanged |= writeToRelay(storedRelays[i].pin, !storedRelays[i].on);
          storedRelays[i].impulse = -1 * storedRelays[i].impulse;
        } else {                  
          storedRelays[i].impulse += (lastRefreshState - oldRefreshState);          
          if(storedRelays[i].impulse > 0) {                      
            storedRelays[i].impulse = 0;
            hasChanged |= writeToRelay(storedRelays[i].pin, storedRelays[i].on);            
          }          
        }
      }
      
      anyRelayHasChanged |= hasChanged;     
            
      //**************************************************************************Execute any mqtt commands if the relay changed
      if(storedRelays[i].mqtt != "" && hasChanged) {        
        String decodedMqttCommand;       
        int count = 0;
        while((decodedMqttCommand = split(storedRelays[i].mqtt, '\n', count)) != "") {
          count++;
          String topic = split(decodedMqttCommand, ' ', 0);
          String value = splitTail(decodedMqttCommand, ' ', 1, true);
          value.replace("@@on@@", ((storedRelays[i].impulse < 0) ^ storedRelays[i].on) ? "true":"false");        
          mqttSend(mqttClient, "", topic, value, false);
        }        
      }            
    }    
  }
  
  //**************************************************************************If the relay was turned on, make sure to log it  
  if(anyRelayHasChanged) {        
    persistLog(getRelayLogLine());
  }
  
  if(hasPirRelay && motionPresent != wasMotionPresent) {
    wasMotionPresent = motionPresent;
    mqttSend(mqttClient, storedConfig.mqttbasepath, getDeviceUID() + "_motion", motionPresent ? "true":"false", true);
  }  
}

boolean writeToRelay(int pin, boolean on) {
  boolean isOn = getPinMode(pin) == OUTPUT;
  
  if(isOn != on) {
    if(on) {
      pinMode (pin, OUTPUT);        
      digitalWrite(pin, 0);
    } else {    
      pinMode (pin, INPUT);
    } 

    return true;
  }

  return false;
}

boolean moveStepper(unsigned long steps,  unsigned long startperiodUs, unsigned long targetperiodUs, unsigned long accgainUs, bool forward) {
  if(storedConfig.stepperenablepin > 0) {      
    digitalWrite(storedConfig.stepperenablepin, LOW);
  }  
  if(storedConfig.stepperdirpin > 0) {
    digitalWrite(storedConfig.stepperdirpin, forward ? HIGH : LOW);      
  }  
  if(storedConfig.steppersteppin > 0) {
      ESP.wdtDisable();      
      unsigned long currentPeriod = max(startperiodUs, targetperiodUs);

            
      for(unsigned long i = 1; i <= steps; i++){
        digitalWrite(storedConfig.steppersteppin, HIGH);              
        digitalWrite(storedConfig.steppersteppin, LOW);        
        currentPeriod = max(targetperiodUs, currentPeriod - accgainUs);
      }
      
      ESP.wdtEnable(WDT_TIME);
  }        
}

unsigned long loopStart = 0;
int loopPos = 0;
AutomataStep *currentLoop;  
int currentCount;

void loopAutomata() {  
  if(loopStart == 0) {       
    currentCount = storedConfig.automataCount;    
    currentLoop = (AutomataStep*)realloc(currentLoop, currentCount * sizeof(AutomataStep));
    memcpy(currentLoop, storedAutomataSteps, currentCount * sizeof(AutomataStep));    
    
    //execute initial instructions before loop start
    while(currentLoop[loopPos].timeOffsetMs == -1) {
      executeMultiInstruction(currentLoop[loopPos].multiInstr);
      loopPos++;
    }
    
    loopStart = millis();
  }    
  unsigned long loopTime = millis() - loopStart;
  
  if(currentCount > loopPos) {
    if(loopTime >= currentLoop[loopPos].timeOffsetMs) {    
      executeMultiInstruction(currentLoop[loopPos].multiInstr);
      loopPos++;
    }
  } else {    
    loopPos = 0;
    loopStart = 0;
  }
}

//Main loop of the application 
void loop(void) {
  
  if(anyRelayHasChanged || configChanged) {       
    configChanged = false;
    anyRelayHasChanged = false;
    
    mqttReport(false, true, false);
  }  
  
  if(reboot) {
    Serial.println("Rebooting...");
    delay(500);
    ESP.restart();
  }
  //handle OTA firmware update
  loopOta();    
  
  //hanlde autonomous behavior 
  if(storedConfig.automataCount > 0) {
    loopAutomata();
  }  
    
  //handle MQTT stuff
  if(mqttConfigured()) {
    if(!mqttClient->connected()) {
      mqttstate = MQTT_DISCONNECTED;
      mqttEstablish();
    } else {
      mqttstate = MQTT_CONNECTED;
      mqttClient->loop();
    }    
  }
  
  //update the clock  
  readClock();  

  //check for motion present
  motionSense();
      
  //do heart beat actions 
  if(heartBeat) {    
    executeHeartBeat();    
  }

  //update the state
  refreshState();
}
