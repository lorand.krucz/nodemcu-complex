struct Relay {    
  int pin;
  char name[20];
  
  boolean schedule;
  boolean pir;  
  boolean heat;
  boolean cool;
  int temptarget;

  boolean on;
  long impulse;

  char mqtt[100];
};

struct Schedule {    
  char name[20];
  boolean oneTime;
  boolean onMonday;
  boolean onTuesday;
  boolean onWednesday;
  boolean onThursday;
  boolean onFriday;
  boolean onSaturday;
  boolean onSunday;
  int year;
  int month;
  int date;
  int startHour;
  int startMinute;
  int durationSeconds;  
};

struct RelaySchedule {
  int relayId;
  int scheduleId; 
};

struct AutomataStep {
  long timeOffsetMs;
  char multiInstr[40];  
};

struct Config {  
  char identity[40];
  char metadata[40];
  char timezone[40];     
  int piezopin;
  int pirpin;
  int dhtpin;
  int steppersteppin;
  int stepperdirpin;
  int stepperenablepin;

  char mqtturl[50];
  int  mqttport;
  char mqttuser[100];
  char mqttpass[40];
  char mqttbasepath[40];
  char mqttcmdchannel[40];
  char mqttcmdjson[40];
  
  boolean allwaysap;
  char ssid[32];
  char pass[32];
  
  char ip[16];
  char gateway[16];
  char dns[16];
  char netmask[16];
    
  int relayCount;
  int scheduleCount;  
  int relayScheduleCount;
  int automataCount;
};
