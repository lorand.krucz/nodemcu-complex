#include <PubSubClient.h>

#define MQTT_BUFFER_SIZE 4096

void mqttPrintStart(PubSubClient *client, const char* topic, int length, bool retain) {
  if (client->connected()) {
    client->beginPublish(topic, length, retain);
  }  
}

void mqttPrintf(PubSubClient *client, const char* chunk) {  
  client->print(chunk);
}

void mqttPrintFinish(PubSubClient *client) {
  client->endPublish();
}


//send an json packet to MQTT
void mqttSend(PubSubClient *client, String base, String topic, String value, bool retain) {  
  if (client->connected()) {
    if(!client->publish((char*) (base + topic).c_str(), (char*) value.c_str(), retain)) {
      Serial.println("MQTT Send failed, connected but not sending [" + (base + topic) + "]");      
    } else {
      Serial.println("MQTT Sent [" + (base + topic) + "]");
    }
  }
}

//subscribe to MQTT
void mqttSub(PubSubClient *client, String base, String topic) {
  if (client->connected()) {
    client->subscribe((char*) (base + topic).c_str());
  }
}

boolean mqttConnect(PubSubClient *client, String url, int port, String name, String id, String user, String pass, String base, String cmdchannel, MQTT_CALLBACK_SIGNATURE) {
  if(url != "" && port != -1) {        
    client->setServer((char*) url.c_str(), port);        
    if (client->connect((char*) name.c_str(), (char*) user.c_str(), (char*) pass.c_str(), (char*) String(base + id + "_will").c_str(), 2, true, "Ded", true)) {
      client->setBufferSize(4096);
      client->setCallback(callback);
      if(cmdchannel != id && cmdchannel != "") {
        mqttSub(client, base, cmdchannel);
      }
      mqttSub(client, base, id);
      mqttSend(client, base, id + "_will", "Not Ded", true); 
      return true;      
    } else {
      Serial.println("MQTT State: " + String(client->state()));      
    }
  }
  
  return false;
}
